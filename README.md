# A build system 

*bbb* is a zero configuration build system.

## Requirement

* [Ruby 1.9](http://ruby-lang.org)
* The [term-ansicolor](https://rubygems.org/gems/term-ansicolor) gem (optional)
* [GCC/G++](http://gcc.gnu.org/)

## Install

There is no gem packaging at this time (it will come). Thus, for now, just clone the repo wherever you want :

    git clone https://glejeune@bitbucket.org/glejeune/bbb.git

Then add *bbb/bin* path in your *PATH*

    export PATH=$PATH:/path/to/bbb/bin

## Test

Go in *bbb/example* an just run

    $ bbb

Enjoy!
