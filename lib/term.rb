# frozen_string_literal: true

begin
  require 'rubygems'
  require 'term/ansicolor'
  COLOR = true
rescue LoadError => _e
  COLOR = false
end

module Print
  include Term::ANSIColor if COLOR

  def color_puts(value)
    if value.is_a? Hash
      value.each do |color, data|
        print send(color) if COLOR
        print data
        print reset if COLOR
      end
      print "\n"
    else
      puts value
    end
  end
end
