# frozen_string_literal: true

require 'mkmf'

module Compiler
  C = ['c'].freeze
  CPP = ['cc', 'cpp', 'cxx', 'c++'].freeze

  COMPILATOR = {
    'gcc' => C,
    'g++' => CPP
  }.freeze

  LINKER = {
    lib: ['ar cr', 'ranlib'],
    dylib: ['g++ -shared'],
    exe: ['g++']
  }.freeze

  COMPILATOR.each do |key, _value|
    unless MakeMakefile.find_executable(key)
      puts "#{key} not found..."
      exit 1
    end
  end
end
