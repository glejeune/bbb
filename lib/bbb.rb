# frozen_string_literal: true

require 'rbconfig'
require 'fileutils'
require 'open3'

require_relative('./compiler')
require_relative('./term')

VERBOSE = true

class BBB
  include Print

  def init(root)
    @root_path = root

    @build_path = File.join(@root_path, '.build')
    FileUtils.rm_rf(@build_path) if File.exist?(@build_path)
    Dir.mkdir(@build_path)

    @output_path = File.join(@root_path, 'bin')
    FileUtils.rm_rf(@output_path) if File.exist?(@output_path)
    Dir.mkdir(@output_path)
  end

  def os_definition
    case RbConfig::CONFIG['target_os']
    when /darwin/
      '-D__APPLE__'
    when /win/
      '-D__WIN32__'
    else
      '-D__UNIX__'
    end
  end

  def lib_ext
    case RbConfig::CONFIG['target_os']
    when /darwin/
      '.a'
    when /win/
      '.lib'
    else
      '.a'
    end
  end

  def dylib_ext
    case RbConfig::CONFIG['target_os']
    when /darwin/
      '.dylib'
    when /win/
      '.dll'
    else
      '.so'
    end
  end

  def exe_ext
    case RbConfig::CONFIG['target_os']
    when /darwin/
      ''
    when /win/
      '.exe'
    else
      ''
    end
  end

  def find_includes
    @includes = []
    ['h', 'hpp', 'hxx'].each do |ext|
      @includes += Dir.glob(File.join(@root_path, '**', "*.#{ext}"))
    end
  end

  def get_include_path(inc)
    @includes.each do |inc_path|
      if m = /(.*)#{inc}$/.match(inc_path)
        return "-I #{m[1]}"
      end
    end
  end

  def get_includes(file)
    incs = []
    IO.readlines(file).each do |line|
      if m = /#include\s*"([^"]*)"/.match(line)
        incs << get_include_path(m[1])
      end
    end
    incs
  end

  def compile
    Compiler::COMPILATOR.each do |compiler, exts|
      exts.each do |ext|
        Dir.glob(File.join(@root_path, '**', "*.#{ext}")).each do |file|
          obj = File.join(@build_path, file.gsub(/^#{@root_path}[\/|\\\\]/, '')) + '.o'
          Dir.mkdir(File.dirname(obj)) unless File.exist?(File.dirname(obj))

          cmd = [compiler, os_definition] + get_includes(file) + ['-c', file, '-o', obj]
          color_puts white: 'Compiling ', green: file
          run_command(cmd)
        end
      end
    end
  end

  def run_command(cmd)
    color_puts red: cmd.join(' ') if VERBOSE
    Open3.popen3(cmd.join(' ')) do |_stdin, _stdout, stderr, wait_thr|
      err = stderr.read.chomp
      puts err unless err.empty?
      exit_status = wait_thr.value # Process::Status object returned.
    end
  end

  def link
    libs = []
    dylibs = []
    exes = []

    Dir.glob(File.join(@build_path, '*')).each do |path|
      base = File.basename(path)
      case base
      when /^lib/
        # link lib
        libs << path
      when /^dylib/
        # link dylib
        dylibs << path
      else
        # link exe
        exes << path
      end
    end

    llibs = link_libs(libs)
    llibs += link_dylibs(dylibs)
    link_exes(exes, llibs)
  end

  def link_libs(libs)
    linklibs = []
    libs.each do |lib|
      libname = File.basename(lib)
      output = File.join(@output_path, libname + lib_ext)

      color_puts white: 'Link ', green: output

      files = Dir.glob(File.join(lib, '**', '*'))

      cmd = [Compiler::LINKER[:lib][0], output] + files
      run_command(cmd)

      cmd = [Compiler::LINKER[:lib][1], output]
      run_command(cmd)

      linklibs << "-l#{libname.gsub(/^lib/, '')}"
    end
    linklibs
  end

  def link_dylibs(dylibs)
    linklibs = []
    dylibs.each do |lib|
      color_puts white: 'Link ', green: lib
    end
    linklibs
  end

  def link_exes(exes, llibs)
    exes.each do |exe|
      exename = File.basename(exe)
      output = File.join(@output_path, exename + exe_ext)

      color_puts white: 'Link ', green: output

      files = Dir.glob(File.join(exe, '**', '*'))

      cmd = [Compiler::LINKER[:exe][0]] + files + ['-o', output, "-L#{@output_path}"] + llibs
      run_command(cmd)
    end
  end

  def run(path = '.')
    color_puts blue: "Build from #{path}"
    init(path)
    find_includes
    compile
    link
  end

  def self.build(path = '.')
    new.run(path)
  end
end
